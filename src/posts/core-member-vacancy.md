---
title: "Vacancy: core member (unpaid)"
summary: The foundation is seeking new core team members to jumpstart research and action against suffering. GF will donate €300 for each successful referral.
publishedAt: 2020-03-20
---
_[The post will be updated once the opportunity is no longer valid. See also a referral opportunity at the end.]_

Guardians Foundation (GF), a private initiative of one person, is seeking core team members to jumpstart research and action against suffering.

The role is unpaid and remote (for the time being).

The time commitment is flexible. The work is diverse (although one can specialize) and thus offers opportunities for learning and for collaborating.

We strive for effectiveness, compassion, and intellectual honesty. We are direct with each other and try to be meta in general. We help each other to be effective suffering reducers in the long run, too.

Besides working from the same virtual office, we will have weekly team calls to share updates, give feedback and plan and approve next steps.

The core team will be involved in the **activities** like:
- building a strong community around compassionate transhumanism (including working with the online [community](https://guardians.gdn/#s9))
- proposing, defining and conducting research (especially on social movements and emerging technologies)
- summarizing literature reviews
- consulting third-parties based on the research
- registering the non-profit in a suitable country
- updating the website
- communicating the mission on social media and directly to individuals
- planning live events
- writing and proofreading
- seeking promising collaborations.

One may be **good fit for the role** if they:
- find [abolishing](https://www.abolitionist.com/) and preventing unnecessary suffering as the overriding priority for a society
- familiar with the [ideas](https://www.effectivealtruism.org/articles/prospecting-for-gold-owen-cotton-barratt/) of effective altruism (including [replaceability](https://concepts.effectivealtruism.org/concepts/replaceability/) and [counterfactual](https://concepts.effectivealtruism.org/concepts/counterfactual-considerations/) and [expected-value](https://concepts.effectivealtruism.org/concepts/expected-value-theory/) thinking)
- are comfortable with “high-(resource)-risk, high-(reduced-suffering)-payoff” endeavors
- are a transhumanist (i.e. generally in favour of using advanced technology (potentially including human germline genetic changes) to fix the ills of human Darwinian biology)
- are initiative
- have good English written communication skills
- can and like to work in a team
- are an [antispeciesist](https://www.animal-ethics.org/ethics-animals-section/speciesism/).

Join us in creating an effective think-and-do tank with the mission to “destroy the hell”.

To apply, send us your resume, other format or link that would give us an overview of your skills, knowledge and perhaps motivations. (Use the email from the footer. Questions to the same address are also welcomed.)

Finally, if you know a person who might be a good fit for the role, ask them to mention you if they apply. In case they are selected, GF will donate €300 to one of Animal Charity Evaluators' [Recommended Charities](https://animalcharityevaluators.org/donation-advice/recommended-charities/) of your choosing.
