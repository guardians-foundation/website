---
title: Guardians Project, in short
summary: A summary on Guardians Project, a long-term umbrella project for bootstrapping a compassionate transhuman society.
publishedAt: 2020-03-17
lastUpdatedAt: 2020-03-18
---
## in a nutshell


Guardians Project is:
- development of the long-term vision of a civilization existing explicitly to abolish unnecessary suffering _and_ to guard against its re-emergence
- bootstrapping a dedicated community
- dissemination of the vision
- persisting the vision long-term (including through global catastrophes)


## words


What is Guardians Project (GP)? It is the development of a motivating and self-fulfilling vision of a future compassionate civilization and organizing a committed community around the vision. It is seeding foundation for a future transhuman society dedicated to abolishing and long-term guarding against suffering.

It is answering what we should do after we abolish unnecessary suffering. Can we be sure that suffering will _never_ re-emerge?

The felt-badness and urgency of extreme suffering, its distinct intrinsic disvalue if you will, reveals the responsibility to alleviate and prevent it. In the amoral universe, impartial rational agents have no choice but to [persist](https://www.smashwords.com/books/view/543094) to reduce the odds of worse suffering. This, we suggest, may be the closest thing to the most meaningful purpose we have in the world.

### a society of guardians

We call the proposed society “guardians” to highlight the importance of guarding against suffering in a post-[abolition](https://www.abolitionist.com/) future.
Guardians are a future technologically-advanced society which took control over its evolution with the ultimate aim of benefiting sentience.

Their better judgement is not longer subverted by the baser passions that were genetically adaptive in the environment of their ancient ancestors. And they have achieved better better judgement at that - via enhanced cognition and long, reflective lives.

Liberated from the [tyranny](https://qualiacomputing.com/2016/11/19/the-tyranny-of-the-intentional-object/) of mindless replication, they re-encephalized their old emotions (and designed new ones) to serve the explicitly acknowledged mission - abolishing extreme suffering and staying on guard indefinitely. They are those who haven’t “opted out”; blissful, not "blissed out".

> Aided by synthetic enhancement technologies, fine-textured gradients of intense emotional well-being can play an information-signalling role at least as versatile and sophisticated as gradients of emotional ill-being or pain-sensations today. Simplistically, it may be said that posterity will be “permanently happy”. However, this expression can be a bit misleading. Post-humans are unlikely to be either “blissed out” wireheads or soma-addled junkies. Instead, we may navigate by the gradients of a multi-dimensional compass that’s designed – unlike its bug-ridden Darwinian predecessor – by intelligent agents for their own ends.
>
> — [_Information-Sensitive Gradients of Bliss_](https://qualiacomputing.com/2016/08/03/information-sensitive-gradients-of-bliss/)

### other uses

“Guardians” is also related to the general notion that intelligent agents have the responsibility to help and look after less foresighted and less understanding sentient beings. “Guardians” may be especially helpful as an alternative to the concepts of “owners” and “masters”.

> My piece of advice to give to new vegans would be to urge them to replace the term ‘owner’ with the term ‘guardian’ when referring to their relationship with their animal companions and other kindred beings. So they may not only not eat them, but that they take a proactive role in changing the existing paradigm that sees and treats other species as no more than mere property, objects, resources, commodities, and things.
>
> — Elliot M. Katz ([src](https://www.ourhenhouse.org/2012/12/new-to-veganism-read-this-inspirational-quotes-from-more-than-30-long-time-vegans/))

Not least, for [suffering-leaned](https://magnusvinding.com/2018/09/03/the-principle-of-sympathy-for-intense-suffering/) altruists today, the guardian identity can serve as a powerful mental tool to see the long-term purpose, reduce akrasia and bond with other aspiring guardians / abolitionists.

### potential outcomes, broadly

Success of GP would be bootstrapping an effective guardians entity in the world. The goal is purposefully loose, for any future with civilizational efforts dedicated to abolishing and preventing suffering could count as fulfilling guardians’ mission.

Other outcomes would not necessarily constitute a failure: “just” raising awareness about the problem of suffering and suggesting being explicit about abolishing “unconsentable” suffering could be worth our time[^1]. The same applies to e.g. connecting potential and aspiring suffering reducers and advocating [compassionate transhumanism](https://www.hedweb.com/transhumanism/neojainism.html).

Still, if GP [causes](https://80000hours.org/articles/accidental-harm/) greater suffering than it prevents, the project will be re-assessed and if deemed irrevocable, terminated (as implied by the overarching goal of reducing suffering).

### founding steps

The initial stage (of the first attempt[^2]) of GP is finding more “guardians” in the world and helping them be more effective in their work / lives towards the mission. We particularly aim to raise morale and facilitate better collaboration and discussions in the dispersed group.

To this end we are starting an experimental intentional online [community](https://guardians.gdn/#s9), aimed at utilizing / hacking our human need for a close community to serve the transhumanist purpose. We are also planning a broader network of persons sharing the mission.

(We are also [proposing](https://docs.google.com/document/d/100NjXAciyLszpSSB2ddT4Pd6tlbLRHeYBV-LBoSwbRo/edit?usp=sharing) an abolitionist online magazine as a broader discussion and action platform. The project will be re-evaluated once an initial “guardians” group is formed.)

Besides, we are growing a [volunteer pool](https://forms.gle/F28uSMhHxt3dcPwP8) that also covers GF’s projects beyond GP. Effective work towards abolishing and preventing suffering requires a variety of skills - from writing and communicative sharing to illustration and research. Please consider signing up.

Finally, to inform our further actions, a lot of research and analysis need be done[^3] In this light, GF itself can be seen as a part of GP: much of the planned research at the foundation, once conducted, can potentially suggest better strategies and tactics for GP.

### recap

Guardians Project (GP) is a long-term umbrella project to motivate global recognition of extreme suffering and to urge its abolition and indefinite prevention.

At this stage, GP is planned to develop, explore, and actualize a vision of a future civilization whose _explicit_ raison d'être is to abolish unnecessary suffering and to _guard against it indefinitely_.

GP is just starting and is starting with building a sustainable collaboration with “guardians” and other agents aligned with the mission of abolition and guarding against extreme suffering. Consider joining GP as a [community](https://guardians.gdn/#s9) member or a potential [volunteer](https://forms.gle/F28uSMhHxt3dcPwP8).

[^1]: By “worth our time” we mean worth our resources considering, too, other work we could do instead and how replaceable our work is. (For those not familiar with the concepts: [counterfactual thinking](https://concepts.effectivealtruism.org/concepts/counterfactual-considerations/) and [replaceability](https://concepts.effectivealtruism.org/concepts/replaceability/).)

[^2]: Many iterations are expected, if not re-launches even: GP is a long term and experimental by necessity.
