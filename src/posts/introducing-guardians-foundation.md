---
title: Introducing Guardians Foundation
publishedAt: 2020-03-12
---
What civilizations are possible? Which ones do we wish to become? Are _those_ possible?

A civilization is determined in large part by (behavior of) agents who comprise it. Therefore, “[what we wish to become](https://alexandrnil.gitlab.io/nil-wiki/writing/drafts/what-we-wish-to-become/)” should be driven by what civilization we, on our best judgement, need in the world[^1].

Those who hold that alleviation and prevention of extreme suffering should be a priority of a civilization[^2], may wonder if humanity can give rise to such a civilization and if so, how we can make it more likely.

The question of the possibility of, and bootstrapping, a society explicitly living out suffering-focused ethics are the focus area of Guardians Foundation (GF). The organization is being set up to support, and conduct its own, projects related to the vision of a future compassionate transhuman[^3] society.

“Guardians” comes from a vision (hinted in the teaser website [guardians.gdn](https://guardians.gdn/)) of a future civilization whose explicit purpose is to guard sentience against extreme suffering in the [amoral] universe. Guardians take risks of future suffering seriously. Ethically, therefore, they have no choice but to stay on guard indefinitely.

GF, while aspiring to the principles of [effective altruism](https://www.effectivealtruism.org/), takes a more speculative and explorative approach (but still reasonably cautious: as per e.g. 80,000 Hours’ [“Ways people trying to do good accidentally make things worse, and how to avoid them”](https://80000hours.org/articles/accidental-harm/)), as it sees may be justified by unknowns of its long-term perspective, the fresh start, and self-fulfilling prophecy effects.

GF pursues a twin-track approach: 1) it works in the area of societal priorities and movements while 2) building and supporting a community dedicated to abolishing suffering. The latter is viewed as the base for planning and bootstrapping projects of the former kind. GF works both with institutions and with individuals aspiring to the mission.

## Current priorities

Currently, besides setting up, GF prioritizes building an online community / network of suffering-focused transhumanists and defining research topics. In the future we will continue to supplement our research with real-world applications (as per our think-and-do tank approach), including offering evidence-based advising for decision and policy makers.

We have many research and other projects in pre-assessment stage but are constrained by the number of persons involved. In the coming months (optimistically) we will jumpstart our capacity by growing our [volunteer pool](https://forms.gle/eGSZ7TKKjUjkb7Rw5) and by onboarding new core members. Starting the community alone would give us a vital feedback and discussion network.

---

The first step is the hardest.[^4] But the gravity of the suffering problem gives us an overcoming reason to start the foundation and to collaborate towards a future where the cause is explicitly embraced on a societal level.

Onwards,<br>Guardians Foundation

[^1]: And we do need one (on the [inclusive](https://www.smashwords.com/books/view/719903) definition of “we”), for the alternatives are incapable of addressing the suffering risks, and [wild](https://en.wikipedia.org/wiki/Wild_animal_suffering) ones allow the worst torments even.

[^2]: The reduction of suffering as a priority for a future civilization may be supported by more people than one would think: see “What should a future civilization strive for?” in the Future of Life Institute’s public [survey](https://futureoflife.org/superintelligence-survey/) as one piece of evidence.

[^3]: We say “transhuman” because we think that unaugmented humans are highly unlikely to solve the suffering problem.

[^4]: Independent of early success, GF will continue to exist in one form or another: _at minimum_, GF would serve as a label for altruistic activities (grants, volunteering, etc.) of its core members.
