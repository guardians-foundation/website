const pluginRss = require("@11ty/eleventy-plugin-rss");
const markdownIt = require("markdown-it");
const markdownItFootnote = require("markdown-it-footnote");
const { DateTime } = require("luxon");

const P_TAG = '<p>';
const P_TAG_CLOSING = '</p>';


module.exports = function(config) {
	config.addPlugin(pluginRss);

	config.setLibrary('md',
		markdownIt({ html: true }).use(markdownItFootnote));

	config.addFilter('htmlDateString', (dateObj) => {
    return DateTime.fromJSDate(dateObj, {zone: 'utc'}).toFormat('yyyy-LL-dd');
  });
	config.addFilter("readableDate", (dateObj) => {
    return DateTime.fromJSDate(dateObj, {zone: 'utc'}).toFormat("dd LLL yyyy");
  });

	config.addShortcode('summary', extractSummary);

	// config.addCollection('tagList', getTagList);

  config.addPassthroughCopy('src/admin');
  config.addPassthroughCopy('src/imgs/uploads');
  return {
    dir: {
      input: "src",
      output: "dist",
      includes: "_includes",
      layouts: "_layouts",
      data: "_data"
    }
    // passthroughFileCopy: true
  };
};


function extractSummary({ data, templateContent }) {
	if (data.summary) {
		return data.summary
	}

	const indexPTag = templateContent.indexOf(P_TAG);
	const indexPTagClosing = templateContent.indexOf(P_TAG_CLOSING);
	if (indexPTag === -1 || indexPTagClosing === -1) {
		return '';
	}
	return templateContent.substring(
		indexPTag + P_TAG.length,
		indexPTagClosing,
	);
}


function getTagList(collection) {
  const tagSet = new Set();
  collection.getAll().forEach((item) => {
    if( "tags" in item.data ) {
      const tags = item.data.tags.filter((item) => {
        switch(item) {
          // this list should match the `filter` list in tags.njk
          case "all":
          case "nav":
          case "post":
          case "posts":
            return false;
        }

        return true;
      });

      for (const tag of tags) {
        tagSet.add(tag);
      }
    }
  });

  return [...tagSet];
}
